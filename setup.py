#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import join as pjoin

from setuptools import setup, find_namespace_packages


with open(pjoin("evoclearn", "plt", "version.py")) as infh:
    version_py = {}
    exec(infh.read(), version_py)
    package_name = version_py["package_name"]
    version = version_py["__version__"]


python_requires = ">=3.7"


install_requires = [
    "matplotlib",
    "svgpathtools>=1.5.0,<1.6.0",
    "cairosvg>=2.5.2,<2.6.0",
    "evoclearn-core>=0.18.6,<0.19.0",
]


extras_require = {
    "dev": ["ipython"]
}


setup_args = {
    "name": package_name,
    "version": version,
    "description": "Plotting tools for Early Vocal Learning simulations.",
    "long_description": "Plotting tools for Early Vocal Learning simulations...",
    "author": "The EVocLearn Group",
    "author_email": "daniel.r.vanniekerk@gmail.com",
    "url": "https://gitlab.com/evoc-learn-group/evoc-learn-plt",
    "packages": find_namespace_packages(include=["evoclearn.*"]),
    "python_requires": python_requires,
    "install_requires": install_requires,
    "extras_require": extras_require
}

setup(**setup_args)
